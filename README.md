# bad_chars

This repo has files with names that cause issues, as documented in https://gitlab.com/gitlab-org/gitlab/-/issues/323082

```shell
  echo 'bad' > $'\220'foo
  echo 'bad' > $'\220'
  echo 'bad' > no_bad
  git add -A
  git commit -m "add files with filenames with bad chars"
```
